//compile with:
// g++ -std=c++11 ./src/main.cpp -o ./bin/audio_test -I ./include -L ./lib -lopenal -lpthread -ldl -lsndio

#include <iostream>

#include <AL/al.h>
#include <AL/alc.h>

using namespace std;

int main(int argc, char** argv){
	cout << "BEGIN\n";
	ALCdevice *device;

	device = alcOpenDevice(NULL);
	if (!device){
		cout << "ERROR OPENING OPENAL DEVICE\n\n";
	}
	
	ALCcontext *context;
	context = alcCreateContext(device, NULL);
	if (!alcMakeContextCurrent(context)){
		cout << "ERROR CREATING OPENAL CONTEXT\n\n";
	}
	
	ALuint source;
	alGenSources(1, &source);
	alSourcef(source, AL_PITCH, 1);
	alSourcef(source, AL_GAIN, 1);
	alSource3f(source, AL_POSITION, 0, 0, 0);
	alSource3f(source, AL_VELOCITY, 0, 0, 0);
	alSourcei(source, AL_LOOPING, AL_FALSE);
	
	ALuint buffer;
	alGenBuffers(1, &buffer);
	
	const int SIZE = 100000;
	unsigned char data[SIZE];
	for(int i = 0; i < SIZE; i++){
		data[i] = i % 256;
	}
	
	
	alBufferData(buffer, AL_FORMAT_MONO8, data, SIZE, 100000);
	alSourcei(source, AL_BUFFER, buffer);
	alSourcePlay(source);
	
	cin.get();
		
	alDeleteSources(1, &source);
	alDeleteBuffers(1, &buffer);
	alcDestroyContext(context);
	alcCloseDevice(device);
	return 0;
}
